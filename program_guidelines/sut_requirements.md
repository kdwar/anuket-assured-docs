# System Under Compliance Verification Requirements

The compliance verification environment (hardware and software) is defined
by the applicable project TSC.

## Similarity Policy

Platforms identified as similar to platforms that have previously been awarded the
program mark may apply to also use the program marks.  The CVC can
decide to grant “Anuket Assured” to products designated “under similarity”. The
committee will consider similarity in the following areas (for example):

* Compute
* Network
* Storage
* Software / Stack

Hardware platforms receiving rights to use the program marks “under similarity”
will be so designated on the website.
